package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

const BlockSize = 4 // Configurable number of transactions in a block
const BlockFile = "blocks.txt"
const LevelDBPath = "mydb" // Path to the LevelDB database

type Transaction struct {
	TxnID    string
	Value    int
	Version  float64
	Validity bool
}

type BlockStatus int

const (
	Committed BlockStatus = iota
	Pending
	PreviousBlockHash
)

type Block struct {
	BlockNumber       int
	Transactions      []Transaction
	Timestamp         int64
	Status            BlockStatus
	PreviousBlockHash string
	ProcessingTime    time.Duration
}

type BlockInterface interface {
	PushValidTransactions(transactions []Transaction)
	UpdateStatusToCommitted()
	CalculateHash()
}

func (b *Block) PushValidTransactions(transactions []Transaction, db *leveldb.DB) bool {

	startTime := time.Now()

	var wg sync.WaitGroup
	for i := range transactions {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			// Calculate hash concurrently
			// For simplicity, consider hash calculation as a placeholder function
			hash := calculateHash(transactions[idx])
			// Update transaction ID and validity
			transactions[idx].TxnID = hash
			if transactions[idx].Version == 1.0 {
				transactions[idx].Validity = true
			} else {
				transactions[idx].Validity = false
			}

		}(i)
	}
	wg.Wait()

	// Push transactions to the block
	b.Transactions = transactions
	b.Timestamp = time.Now().Unix()

	// Validate and return true if all transactions are valid
	// for _, txn := range transactions {
	// 	if !b.validateTransactionVersion(txn) {
	// 		return false
	// 	}
	// }

	// Save valid transactions to LevelDB
	for _, txn := range transactions {
		if b.validateTransactionVersion(txn) {
			data, _ := json.Marshal(txn)
			err := db.Put([]byte(txn.TxnID), data, nil)
			if err != nil {
				fmt.Printf("Error saving transaction to LevelDB: %v\n", err)
			}
		}
	}

	endTime := time.Now()
	b.ProcessingTime = endTime.Sub(startTime)

	return true
}

func calculateHash(txn Transaction) string {
	// Placeholder function for hash calculation logic

	data, _ := json.Marshal(txn)
	hash := sha256.Sum256(data)
	return fmt.Sprintf("%x", hash)

	//return fmt.Sprintf("Hash(%s)", txn.TxnID)
}

func (b *Block) validateTransactionVersion(txn Transaction) bool {
	// Add your version validation logic here
	// For example, you can check if the transaction version matches the expected version.
	// Modify this logic as per your requirements.
	return txn.Version == 1.0
}

func (b *Block) UpdateStatusToCommitted() {
	b.Status = Committed
}

func (b *Block) CalculateHash() {
	// Calculate the hash of the previous block's information
	previousBlockHash := calculateHash(Transaction{
		TxnID:    b.PreviousBlockHash,
		Value:    len(b.Transactions),
		Version:  float64(b.Timestamp),
		Validity: true,
	})

	// Calculate the hash of the current block's data
	blockData, _ := json.Marshal(b)
	currentBlockHash := calculateHash(Transaction{
		TxnID:    string(blockData),
		Value:    len(b.Transactions),
		Version:  float64(b.Timestamp),
		Validity: true,
	})

	// Combine the two hashes to create the final block hash
	combinedHash := previousBlockHash + currentBlockHash

	// Calculate the SHA-256 hash of the combined hash
	hashBytes := sha256.Sum256([]byte(combinedHash))

	// Set the block's PreviousBlockHash to the final hash
	b.PreviousBlockHash = fmt.Sprintf("%x", hashBytes)
}

func main() {

	// Open the LevelDB
	db, err := openLevelDB()
	if err != nil {
		fmt.Printf("Error opening LevelDB: %v\n", err)
		return
	}
	defer db.Close()

	// Read existing blocks from the file and find the maximum BlockNumber
	maxBlockNumber := readExistingBlocks()
	var previousBlockHash string // Initialize previousBlockHash

	// Input transaction list as a JSON array
	inputJSON := `[{"SIM1": {"val": 2, "ver": 1.0}},
                   {"SIM2": {"val": 3, "ver": 1.0}},
                   {"SIM3": {"val": 4, "ver": 1.0}},
                   {"SIM4": {"val": 5, "ver": 1.0}},
                   {"SIM5": {"val": 6, "ver": 1.0}},
                   {"SIM6": {"val": 7, "ver": 1.0}},
                   {"SIM7": {"val": 8, "ver": 1.0}},
                   {"SIM8": {"val": 9, "ver": 2.0}},
                   {"SIM9": {"val": 10, "ver": 1.0}},
                   {"SIM10": {"val": 11, "ver": 1.0}}]`

	// Parse the JSON array into a slice of maps
	var inputTxns []map[string]map[string]interface{}
	if err := json.Unmarshal([]byte(inputJSON), &inputTxns); err != nil {
		fmt.Println("Error parsing input JSON:", err)
		return
	}

	// Convert the input transactions into Transaction objects
	var transactions []Transaction
	for _, txnData := range inputTxns {
		for txnID, txnInfo := range txnData {
			value, _ := txnInfo["val"].(float64) // Change to float64
			version, _ := txnInfo["ver"].(float64)
			// For simplicity, consider all transactions as valid
			validity := true

			txn := Transaction{
				TxnID:    txnID,
				Value:    int(value), // Convert to int
				Version:  version,
				Validity: validity,
			}

			transactions = append(transactions, txn)
		}
	}

	// Create a channel to receive blocks to be written to a file
	blockChannel := make(chan *Block)

	// Create a wait group to ensure all goroutines are done processing blocks
	var wg sync.WaitGroup

	// Process transactions in batches based on BlockSize
	for i := 0; i < len(transactions); i += BlockSize {
		end := i + BlockSize
		if end > len(transactions) {
			end = len(transactions)
		}

		block := &Block{
			BlockNumber:       maxBlockNumber + i/BlockSize + 1,
			Transactions:      []Transaction{},
			Timestamp:         0,
			Status:            Pending,
			PreviousBlockHash: previousBlockHash, // Set the previous block hash
		}

		if block.PushValidTransactions(transactions[i:end], db) {
			block.UpdateStatusToCommitted()
			fmt.Printf("Block #%d: Processing Time: %s, %+v\n", block.BlockNumber, block.ProcessingTime, block)

			// Calculate the hash for the current block
			block.CalculateHash()

			// Update previousBlockHash for the next block
			previousBlockHash = block.PreviousBlockHash

			// Send the block to the channel
			wg.Add(1)
			go func(b *Block) {
				defer wg.Done()

				// Send the block to the channel
				blockChannel <- b
			}(block)
		} else {
			fmt.Printf("Block #%d: Block was not committed.\n", block.BlockNumber)
		}
	}

	// Close the block channel when done
	go func() {
		wg.Wait()           // Wait for all goroutines to finish
		close(blockChannel) // Close the channel
	}()

	// Now you can have a separate goroutine to receive and process blocks from the channel
	go func() {

		// Create or open a file for appending blocks
		file, err := os.OpenFile(BlockFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Printf("Error opening or creating the file: %v\n", err)
			return
		}
		defer file.Close()

		for receivedBlock := range blockChannel {
			// Process the received block (e.g., write it to a file)
			blockData, err := json.Marshal(receivedBlock)
			if err != nil {
				fmt.Printf("Error encoding block data: %v\n", err)
				continue
			}

			// Append the block data to the file
			if _, err := file.Write(append(blockData, '\n')); err != nil {
				fmt.Printf("Error writing block data to the file: %v\n", err)
			}
		}
	}()

	// Fetch and print details of all blocks from the file
	fetchAndPrintAllBlocks()
	displayLevelDBContents(db)

	// Wait for all goroutines to finish before exiting
	wg.Wait()

}

// Function to read existing blocks from the file and find the maximum BlockNumber
func readExistingBlocks() int {
	file, err := os.Open(BlockFile)
	if err != nil {
		fmt.Println("Error opening the file:", err)
		return 0
	}
	defer file.Close()

	maxBlockNumber := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var block Block
		if err := json.Unmarshal([]byte(scanner.Text()), &block); err == nil {
			if block.BlockNumber > maxBlockNumber {
				maxBlockNumber = block.BlockNumber
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading the file:", err)
	}

	return maxBlockNumber
}

// Function to fetch and print details of all blocks from the file
func fetchAndPrintAllBlocks() {
	file, err := os.Open(BlockFile)
	if err != nil {
		fmt.Println("Error opening the file:", err)
		return
	}
	defer file.Close()

	fmt.Println("Details of all blocks:")
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var block Block
		if err := json.Unmarshal([]byte(scanner.Text()), &block); err == nil {
			fmt.Printf("Block #%d: Processing Time: %s, %+v\n", block.BlockNumber, block.ProcessingTime, block)
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading the file:", err)
	}
}

func openLevelDB() (*leveldb.DB, error) {
	options := &opt.Options{
		ErrorIfMissing: false, // Create the database if it doesn't exist
	}

	db, err := leveldb.OpenFile(LevelDBPath, options)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func displayLevelDBContents(db *leveldb.DB) {
	iter := db.NewIterator(nil, nil)
	defer iter.Release()

	fmt.Println("Contents of the LevelDB:")
	for iter.Next() {
		key := iter.Key()
		value := iter.Value()
		fmt.Printf("Key: %s, Value: %s\n", key, value)
	}

	if err := iter.Error(); err != nil {
		fmt.Printf("Error iterating LevelDB: %v\n", err)
	}
}

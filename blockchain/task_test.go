package main

import (
	"bytes"
	"io"
	"os"
	"testing"
	"fmt"

	"github.com/stretchr/testify/assert"
	"github.com/syndtr/goleveldb/leveldb"
)

func TestPushValidTransactions(t *testing.T) {
	// Initialize a mock LevelDB for testing
	db, _ := leveldb.OpenFile("testdb", nil)
	defer db.Close()

	block := &Block{
		BlockNumber:       1,
		PreviousBlockHash: "prevBlockHash",
	}

	transactions := []Transaction{
		{
			TxnID:    "txn1",
			Value:    10,
			Version:  1.0,
			Validity: false, // Invalid transaction
		},
		{
			TxnID:    "txn2",
			Value:    20,
			Version:  1.0,
			Validity: true,
		},
	}

	valid := block.PushValidTransactions(transactions, db)

	//  PushValidTransactions function returns the expected result
	assert.True(t, valid)

	// Transactions field in the block is set correctly
	assert.Equal(t, transactions, block.Transactions)
}

func TestValidateTransactionVersion(t *testing.T) {
	block := &Block{}

	transaction := Transaction{
		TxnID:    "testTxn",
		Value:    100,
		Version:  1.0, // Valid version
		Validity: true,
	}

	valid := block.validateTransactionVersion(transaction)

	//validation is as expected
	assert.True(t, valid)
}

func TestUpdateStatusToCommitted(t *testing.T) {
	block := &Block{}

	block.UpdateStatusToCommitted()

	//block's status is updated as expected
	assert.Equal(t, Committed, block.Status)
}

func TestCalculateHash(t *testing.T) {
	block := &Block{
		PreviousBlockHash: "prevBlockHash",
		Transactions: []Transaction{
			{
				TxnID:    "txn1",
				Value:    10,
				Version:  1.0,
				Validity: true,
			},
			{
				TxnID:    "txn2",
				Value:    20,
				Version:  1.0,
				Validity: true,
			},
		},
	}

	block.CalculateHash()

	expectedHash := "972fdcc7b9be4608cef4ba9723c7f4ef286210a47146de757c0c28ede77df905"

	//  hash calculation is as expected
	assert.Equal(t, expectedHash, block.PreviousBlockHash)
}

func TestReadExistingBlocks(t *testing.T) {
	// Create a test "blocks.txt" file with example blocks
	testFile, _ := os.Create("test_blocks.txt")
	defer testFile.Close()

	_, err := testFile.WriteString(`{"BlockNumber":1,"Transactions":[],"Timestamp":0,"Status":0,"PreviousBlockHash":"","ProcessingTime":0}`)
	if err != nil {
		t.Fatal(err)
	}

	// Call the function to read existing blocks and get the maximum block number
	maxBlockNumber := readExistingBlocks()

	// Check if the function returns the expected maximum block number
	assert.Equal(t, 3, maxBlockNumber)
}

func TestFetchAndPrintAllBlocks(t *testing.T) {
	// Create a test "blocks.txt" file with some example blocks for testing
	testFile, _ := os.Create("test_blocks.txt")
	defer testFile.Close()

	// Create example block data as JSON and write it to the test file
	exampleBlockData := `{"BlockNumber":1,"Transactions":[],"Timestamp":0,"Status":0,"PreviousBlockHash":"","ProcessingTime":0}`
	_, err := testFile.WriteString(exampleBlockData)
	if err != nil {
		t.Fatal(err)
	}

	// Redirect stdout to capture printed output
	origStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	fetchAndPrintAllBlocks()

	// Close the write end of the pipe and revert stdout
	w.Close()
	os.Stdout = origStdout

	// Read the printed output from the read end of the pipe
	var printedOutput string
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		printedOutput = buf.String()
	}()

	// Verify that the printed output contains the expected block details
	assert.Contains(t, printedOutput, "Block #1: Processing Time: 0s")
}


func TestDisplayLevelDBContents(t *testing.T) {
	// Use the displayLevelDBContents function from your main code
	// Initialize a mock LevelDB for testing
	db, _ := leveldb.OpenFile("testdb", nil)
	defer db.Close()

	// Add some test data to the LevelDB
	key := []byte("test_key")
	value := []byte("test_value")
	err := db.Put(key, value, nil)
	assert.NoError(t, err)

	// Redirect stdout to capture printed output
	origStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	displayLevelDBContents(db)

	// Close the write end of the pipe and revert stdout
	w.Close()
	os.Stdout = origStdout

	// Read the printed output from the read end of the pipe
	var printedOutput string
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		printedOutput = buf.String()
	}()

	// Check if the printed output contains expected information
	assert.Contains(t, printedOutput, "Key: test_key, Value: test_value")
}


func TestMainFunction(t *testing.T) {

	//LevelDB and its functions
	mockDB, _ := leveldb.OpenFile("testdb", nil)
	defer mockDB.Close()
	db, err := openLevelDB()
if err != nil {
    fmt.Printf("Error opening LevelDB: %v\n", err)
    return
}
defer db.Close()

displayLevelDBContents(db)

	testFile, _ := os.Create("test_blocks.txt")
	defer testFile.Close()
	testFile.WriteString("{\"BlockNumber\": 1, \"Transactions\": [], \"Timestamp\": 0, \"Status\": 0, \"PreviousBlockHash\": \"\", \"ProcessingTime\": 0}\n")

	// Capture and check printed output
	origStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	main()

	// Close the write end of the pipe and revert stdout
	w.Close()
	os.Stdout = origStdout

	// Read the printed output from the read end of the pipe
	var printedOutput string
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		printedOutput = buf.String()
	}()

	//  printed output contains expected information
	assert.Contains(t, printedOutput, "Block #1: Processing Time: 0s")
}

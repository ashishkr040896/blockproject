package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"crypto/sha256"
	"encoding/json"

	"google.golang.org/grpc"
	pb "task/transaction" 
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

type TransactionService struct {
	db *leveldb.DB // LevelDB instance
}

func NewTransactionService() (*TransactionService, error) {
	// Initialize LevelDB
	options := &opt.Options{
		ErrorIfMissing: false, // Create the database if it doesn't exist
	}

	db, err := leveldb.OpenFile("mydb", options)
	if err != nil {
		return nil, err
	}

	return &TransactionService{
		db: db,
	}, nil
}

func (s *TransactionService) ProcessTransaction(ctx context.Context, req *pb.TransactionRequest) (*pb.TransactionResponse, error) {
	// Implement the transaction processing logic here

	txn := Transaction{
		TxnID:    req.TransactionId,
		Value:    int(req.Value),
		Version:  float64(req.Version),
		Validity: false, // You need to implement the validity check
	}

	// Calculate the hash of the transaction
	hash := calculateHash(txn)

	// Update transaction ID and validity
	txn.TxnID = hash
	if txn.Version == 1.0 {
		txn.Validity = true
	}

	// Save the transaction to LevelDB
	if err := s.saveToLevelDB(txn); err != nil {
		return nil, err
	}

	// Return a response with a message indicating the result
	return &pb.TransactionResponse{
		Message: "Transaction processed and saved successfully",
	}, nil
}

func (s *TransactionService) saveToLevelDB(txn Transaction) error {
	data, err := json.Marshal(txn)
	if err != nil {
		return err
	}

	return s.db.Put([]byte(txn.TxnID), data, nil)
}

func main() {
	listen, err := net.Listen("tcp", ":8080") // Replace with your desired port
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	service, err := NewTransactionService()
	if err != nil {
		log.Fatalf("Failed to initialize TransactionService: %v", err)
	}

	grpcServer := grpc.NewServer()
	pb.RegisterTransactionServiceServer(grpcServer, service)

	log.Println("gRPC server is listening on :8080")
	if err := grpcServer.Serve(listen); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}

type Transaction struct {
	TxnID    string
	Value    int
	Version  float64
	Validity bool
}

func calculateHash(txn Transaction) string {
	// Marshal the transaction data into a JSON string
	data, err := json.Marshal(txn)
	if err != nil {
		// Handle the error appropriately
		return ""
	}

	// Calculate the SHA-256 hash of the JSON data
	hashBytes := sha256.Sum256(data)

	// Convert the hash bytes to a hexadecimal string
	hash := fmt.Sprintf("%x", hashBytes)

	return hash
}

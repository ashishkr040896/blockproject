package main

import (
    "context"
    "log"
    "net/http"

    "github.com/grpc-ecosystem/grpc-gateway/runtime"
    "google.golang.org/grpc"
    "google.golang.org/grpc/status"
    "google.golang.org/grpc/codes"
    "github.com/gorilla/mux"

    pb "path-to-your-protobuf-generated-code"
)

func Run() error {
    ctx := context.Background()
    mux := runtime.NewServeMux()
    opts := []grpc.DialOption{grpc.WithInsecure()}

    err := pb.RegisterTransactionServiceHandlerFromEndpoint(ctx, mux, "localhost:8080", opts)
    if err != nil {
        return err
    }

    r := mux.NewRouter()
    r.Handle("/swagger/", http.StripPrefix("/swagger/", http.FileServer(http.Dir("./")))
    r.PathPrefix("/").Handler(mux)

    return http.ListenAndServe(":8081", r)
}

func main() {
    if err := Run(); err != nil {
        log.Fatal(err)
    }
}

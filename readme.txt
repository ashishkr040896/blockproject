protoc --go_out=. transactions.proto
go run file.go


task/
├── go.mod
├── go.sum
├── task.go
├── task_test.go
├── mydb
├── transaction/
│   └──transactions.proto/ transactions.pb.go
└── server/
    └── server.go
